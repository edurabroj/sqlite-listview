package com.eduardorabanal.app.sqliteapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by USER on 07/11/2017.
 */
public class DbHelper extends SQLiteOpenHelper {
    final static String TABLE_NAME = "curso";
    final static String ID_CURSO = "id";
    final static String NOMBRE = "nombre";
    final static String CREDITOS = "creditos";
    final static String[] COLUMNAS = {ID_CURSO,NOMBRE,CREDITOS};

    //columnas para usar en el cursor (asignando el alias "_id" al campo ID_CURSO xq el cursorAdapter lo necesita")
    final static String[] COLUMNAScursor = {ID_CURSO + " _id",NOMBRE,CREDITOS};

    final private String CREATE_TABLE_CURSO =
            "CREATE TABLE " + TABLE_NAME + "(" +
                ID_CURSO + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NOMBRE + " TEXT NOT NULL, " +
                CREDITOS + " INTEGER)";

    final private static String BD_NAME = "bd_academica";
    final private static Integer VERSION = 1;
    final private Context context;

    public DbHelper(Context context) {
        super(context, BD_NAME, null, VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_CURSO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    void deleteDB(){
        context.deleteDatabase(BD_NAME);
    }
}
