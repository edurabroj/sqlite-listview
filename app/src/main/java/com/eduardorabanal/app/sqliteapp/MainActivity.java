package com.eduardorabanal.app.sqliteapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.opengl.GLDebugHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private SQLiteDatabase db = null;
    private SQLiteOpenHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DbHelper(this);

        //SQLiteDatabase.deleteDatabase("bd_academica");
        db = dbHelper.getWritableDatabase();

        //final TextView text = (TextView) findViewById(R.id.text);
        final ListView lv = (ListView) findViewById(R.id.lv);

        eliminarFilas();
        insertarCursos();
        actualizarCurso();
        eliminarCurso();

        // CURSOR ADAPTER PARA MOSTRAR DATOS EN LISTVIEW
        Cursor cursos = leerCursos();
        cursos.moveToFirst();
        Adaptador adaptador = new Adaptador(this,cursos,false);
        lv.setAdapter(adaptador);

        //RECORRER EL CURSOR MANUALMENTE
        /*String textoCursos = "";
        for(cursos.moveToFirst(); !cursos.isAfterLast(); cursos.moveToNext()){
            String nombre = cursos.getString(1);
            int creditos = cursos.getInt(2);
            textoCursos+= nombre + " " + creditos + "\n";
        }
        text.setText(textoCursos);*/
    }

    private void insertarCursos() {
        ContentValues valores = new ContentValues();

        valores.put(DbHelper.NOMBRE, "SolWeb");
        valores.put(DbHelper.CREDITOS, "5");
        db.insert(DbHelper.TABLE_NAME,null,valores);
        valores.clear();

        valores.put(DbHelper.NOMBRE, "Proy 1");
        valores.put(DbHelper.CREDITOS, "6");
        db.insert(DbHelper.TABLE_NAME,null,valores);
        valores.clear();

        valores.put(DbHelper.NOMBRE, "Redes 2");
        valores.put(DbHelper.CREDITOS, "4");
        db.insert(DbHelper.TABLE_NAME,null,valores);
        valores.clear();

        valores.put(DbHelper.NOMBRE, "POO");
        valores.put(DbHelper.CREDITOS, "4");
        db.insert(DbHelper.TABLE_NAME,null,valores);
        valores.clear();

        valores.put(DbHelper.NOMBRE, "Estructura de Datos");
        valores.put(DbHelper.CREDITOS, "5");
        db.insert(DbHelper.TABLE_NAME,null,valores);
        valores.clear();
    }

    private Cursor leerCursos(){
        return db.query(DbHelper.TABLE_NAME,DbHelper.COLUMNAScursor,null,new String[]{},"","","");
    }

    private void eliminarFilas(){
        db.delete(DbHelper.TABLE_NAME, null,null);
    }

    private void actualizarCurso(){
        ContentValues valores = new ContentValues();
        valores.put(DbHelper.NOMBRE, "Seguridad Informática");
        db.update(DbHelper.TABLE_NAME, valores, DbHelper.NOMBRE + "=?",new String[]{"Redes 2"});
    }

    private void eliminarCurso(){
        db.delete(DbHelper.TABLE_NAME,DbHelper.NOMBRE + "=?",new String[]{"Proy 1"});
    }
}
