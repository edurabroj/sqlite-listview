package com.eduardorabanal.app.sqliteapp;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by USER on 04/12/2017.
 */
public class Adaptador extends CursorAdapter {
    public Adaptador(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.item, viewGroup, false); // <- layout de item
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvNombre = (TextView) view.findViewById(R.id.tvNombre);
        tvNombre.setText(cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.NOMBRE))); // <- nombre de la columna

        TextView tvCreditos = (TextView) view.findViewById(R.id.tvCreditos);
        tvCreditos.setText(cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.CREDITOS)));
    }
}
